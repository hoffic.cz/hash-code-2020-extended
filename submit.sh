#!/usr/bin/env bash

rm data/code.zip;
zip -r data/code.zip ./ -x "*.gradle*" -x "*.idea*" -x "*build/*" -x "*data/*" -x "*.git*";
