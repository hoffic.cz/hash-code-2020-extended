package dev.hoffic.hc20e.logic;

import dev.hoffic.hc20e.entity.Book;
import dev.hoffic.hc20e.entity.Library;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class Picker {
    public Library best(
            int daysUntilDeadline,
            Set<Library> libraries,
            Set<Book> scannedBooks
    ) {
        long bestScore = -1;
        Library bestLibrary = null;

        for (Library library : libraries) {
            var score = reward(daysUntilDeadline, library, scannedBooks);

            if (score > bestScore) {
                bestScore = score;
                bestLibrary = library;
            }
        }

        return bestLibrary;
    }

    public long reward(int daysUntilDeadline, Library library, Set<Book> scannedBooks) {
        var n = numBooksInTime(daysUntilDeadline, library);
        var feasibleBooksInLibrary = pickNBest(n, library, scannedBooks);

        long reward = 0;
        for (Book book : feasibleBooksInLibrary) {
            reward += book.score;
        }

        return reward * 10000 / library.timeToSignUp;
    }

    public int numBooksInTime(int daysUntilDeadline, Library library) {
        double workingDaysUntilDeadline = daysUntilDeadline - library.timeToSignUp;
        double productiveScanningDays = 1.0 * library.books.size() / library.dailyLimit;

        double operationalTime = Math.min(
                workingDaysUntilDeadline,
                productiveScanningDays
        );

        return (int) Math.floor(operationalTime * library.dailyLimit);
    }

    public Set<Book> pickNBest(int n, Library library, Set<Book> scannedBooks) {
        PriorityQueue<Book> booksByScore = new PriorityQueue<>();

        for (Book book : library.books.values()) {
            if (!scannedBooks.contains(book)) {
                booksByScore.add(book);
            }
        }

        var nBestBooks = new HashSet<Book>();

        for (int i = 0; i < n; i++) {
            if (booksByScore.isEmpty()) {
                break;
            } else {
                nBestBooks.add(booksByScore.poll());
            }
        }

        return nBestBooks;
    }
}
