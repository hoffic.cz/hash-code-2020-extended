package dev.hoffic.hc20e.logic;

import dev.hoffic.hc20e.entity.Book;
import dev.hoffic.hc20e.entity.Library;
import dev.hoffic.hc20e.entity.World;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class Simulator {
    private Picker picker;
    private World world;

    private int libraryReadyAtDay = 0;
    private Library libraryBeingSignedUp = null;

    public Simulator() {
        picker = new Picker();
    }

    public void simulate() {
        int reportingPeriod = world.deadline / 100;
        if (reportingPeriod <= 0) {
            reportingPeriod = 1;
        }
        Instant start = Instant.now();
        while (true) {
            if (world.currentDay % reportingPeriod == 0 && world.currentDay > 0) {
                int percentDone = world.currentDay / reportingPeriod;
                long timeElapsed = Duration.between(start, Instant.now()).toMillis();
                long predicted = timeElapsed / percentDone * 100;
                long left = predicted - timeElapsed;

                System.out.printf("Day %d - %d%% - %ds left%n",
                        world.currentDay,
                        percentDone,
                        left / 1000);
            }

            int daysUntilDeadline = world.deadline - world.currentDay;

            if (daysUntilDeadline < 0) {
                break;
            }

            // Scanning books
            scanBooks(daysUntilDeadline);

            // Choosing libraries to be signed up
            pickLibrary(daysUntilDeadline);

            world.currentDay++;
        }
    }

    private void scanBooks(int daysUntilDeadline) {
        List<Library> emptyLibraries = new LinkedList<>();

        // Collect insights from the libraries
        var availableBooks = new PriorityQueue<Book>();
        var bookCounts = new HashMap<Book, Integer>();
        var bookLocations = new HashMap<Book, Library>();
        for (Library library : world.activeLibraries) {
            if (library.books.isEmpty()) {
                emptyLibraries.add(library);
            } else {
                for (Book book : library.books.values()) {
                    if (!bookCounts.containsKey(book)) {
                        bookCounts.put(book, 1);
                        availableBooks.add(book);
                        bookLocations.put(book, library);
                    } else {
                        var count = bookCounts.get(book);
                        bookCounts.put(book, count + 1);
                    }
                }
            }
        }

        // While there are limited books that score well
        while (true) {
            var capacityLeft = 0;
            for (Library library : world.activeLibraries) {
                capacityLeft += library.dailyLimit;
            }
            var limitedBooks = new LinkedList<Book>();
            var otherBooks = new LinkedList<Book>();

            while (!availableBooks.isEmpty() && capacityLeft > 0) {
                var book = availableBooks.poll();

                if (bookCounts.get(book) == 1) {
                    limitedBooks.add(book);
                } else {
                    otherBooks.add(book);
                }

                capacityLeft--;
            }

            if (limitedBooks.isEmpty()) {
                break;
            }

            for (Book book : limitedBooks) {
                var library = bookLocations.get(book);
                library.scannedBooks.add(book);
                world.scannedBooks.add(book);
                library.books.remove(book.id);
            }
        }

        for (Library library : emptyLibraries) {
            world.activeLibraries.remove(library);
        }
        emptyLibraries.clear();

        // scan non-limited books
        for (Library library : world.activeLibraries) {
            if (library.books.isEmpty()) {
                emptyLibraries.add(library);
            } else {
                var n = picker.numBooksInTime(daysUntilDeadline, library);
                var batchSize = Math.min(n, library.dailyLimit);

                var bestBooks = picker.pickNBest(batchSize, library, world.scannedBooks);

                library.scannedBooks.addAll(bestBooks);
                world.scannedBooks.addAll(bestBooks);

                for (Book book : bestBooks) {
                    library.books.remove(book.id);
                }
            }
        }

        for (Library library : emptyLibraries) {
            world.activeLibraries.remove(library);
        }
    }

    private void pickLibrary(int daysUntilDeadline) {
        // Picking what library to sign up
        if (libraryReadyAtDay >= world.currentDay) {
            if (libraryBeingSignedUp != null) {
                world.signedUpLibraries.add(libraryBeingSignedUp);
                world.activeLibraries.add(libraryBeingSignedUp);
                world.libraries.remove(libraryBeingSignedUp);
            }

            libraryBeingSignedUp = picker.best(daysUntilDeadline, world.libraries, world.scannedBooks);
            if (libraryBeingSignedUp == null) {
                libraryReadyAtDay = Integer.MAX_VALUE;
            } else {
                libraryReadyAtDay = world.currentDay + libraryBeingSignedUp.timeToSignUp;
            }
        }
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
