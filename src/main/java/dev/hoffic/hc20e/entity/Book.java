package dev.hoffic.hc20e.entity;

public class Book implements Comparable<Book> {
    public int id;
    public int score;

    @Override
    public boolean equals(Object o) {
        return o instanceof Book && this.id == ((Book) o).id;
    }

    @Override
    public int hashCode() {
        return id;
    }


    @Override
    public int compareTo(Book o) {
        return o.score - this.score;
    }
}
