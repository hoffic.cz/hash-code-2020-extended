package dev.hoffic.hc20e.entity;

import java.util.*;

public class World {
    public int currentDay = 0;
    public int deadline;

    public Set<Library> libraries;
    public List<Library> activeLibraries;
    public List<Library> signedUpLibraries;

    public Map<Integer, Book> books;
    public Set<Book> scannedBooks;

    public World() {
        libraries = new HashSet<>();
        activeLibraries = new LinkedList<>();
        signedUpLibraries = new LinkedList<>();
        books = new HashMap<>();
        scannedBooks = new HashSet<>();
    }
}
