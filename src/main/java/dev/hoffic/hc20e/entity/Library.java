package dev.hoffic.hc20e.entity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Library {
    public int id;
    public int timeToSignUp;
    public int dailyLimit;

    public Map<Integer, Book> books;
    public List<Book> scannedBooks;

    public Library() {
        books = new HashMap<>();
        scannedBooks = new LinkedList<>();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Library && this.id == ((Library) o).id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
