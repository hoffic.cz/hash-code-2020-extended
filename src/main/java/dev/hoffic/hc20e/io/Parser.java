package dev.hoffic.hc20e.io;

import dev.hoffic.hc20e.entity.Book;
import dev.hoffic.hc20e.entity.Library;
import dev.hoffic.hc20e.entity.World;

import java.io.File;
import java.util.Scanner;

public class Parser {

    public World parse(String problem) throws Exception {
        var world = new World();

        var file = new Scanner(getFile(problem));

        var worldInfoLine = new Scanner(file.nextLine());
        var numberOfBooks = worldInfoLine.nextInt();
        var numberOfLibraries = worldInfoLine.nextInt();
        world.deadline = worldInfoLine.nextInt();

        var bookScoresLine = new Scanner(file.nextLine());
        for (int i = 0; i < numberOfBooks; i++) {
            var book = new Book();
            book.id = i;

            book.score = bookScoresLine.nextInt();

            world.books.put(i, book);
        }

        for (int i = 0; i < numberOfLibraries; i++) {
            var library = new Library();
            library.id = i;

            var libraryInfoLine = new Scanner(file.nextLine());
            var numberOfBooksInLibrary = libraryInfoLine.nextInt();
            library.timeToSignUp = libraryInfoLine.nextInt();
            library.dailyLimit = libraryInfoLine.nextInt();

            var libraryBooksLine = new Scanner(file.nextLine());
            for (int j = 0; j < numberOfBooksInLibrary; j++) {
                var currentBookId = libraryBooksLine.nextInt();
                if (!library.books.containsKey(currentBookId)) {
                    library.books.put(currentBookId, world.books.get(currentBookId));
                }
            }

            libraryInfoLine.close();
            libraryBooksLine.close();

            world.libraries.add(library);
        }

        file.close();
        worldInfoLine.close();
        bookScoresLine.close();

        return world;
    }

    private File getFile(String problem) {
        String filename;

        switch (problem) {
            case "a":
                filename = "data/in/a_example.txt";
                break;
            case "b":
                filename = "data/in/b_read_on.txt";
                break;
            case "c":
                filename = "data/in/c_incunabula.txt";
                break;
            case "d":
                filename = "data/in/d_tough_choices.txt";
                break;
            case "e":
                filename = "data/in/e_so_many_books.txt";
                break;
            case "f":
                filename = "data/in/f_libraries_of_the_world.txt";
                break;
            default:
                throw new RuntimeException();

        }

        return new File(filename);
    }
}
