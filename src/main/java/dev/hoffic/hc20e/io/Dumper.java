package dev.hoffic.hc20e.io;

import dev.hoffic.hc20e.entity.Book;
import dev.hoffic.hc20e.entity.Library;
import dev.hoffic.hc20e.entity.World;

import java.io.File;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class Dumper {
    public void dump(World world, String problem) throws Exception {
        var writer = new PrintWriter(getFile(problem));

        removeEmpty(world.signedUpLibraries);

        writer.println(world.signedUpLibraries.size() + " ");

        for (Library library : world.signedUpLibraries) {
            writer.print(library.id + " ");
            writer.println(library.scannedBooks.size() + " ");

            for (Book book : library.scannedBooks) {
                writer.print(book.id + " ");
            }

            writer.println(" ");
        }

        writer.close();
    }

    private void removeEmpty(List<Library> libraries) {
        List<Library> toRemove = new LinkedList<>();

        for (Library library : libraries) {
            if (library.scannedBooks.isEmpty()) {
                toRemove.add(library);
            }
        }

        for (Library library : toRemove) {
            libraries.remove(library);
        }
    }

    private File getFile(String problem) {
        String filename = "data/out/" + problem + ".txt";

        return new File(filename);
    }
}
