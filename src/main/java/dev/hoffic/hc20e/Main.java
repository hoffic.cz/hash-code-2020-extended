package dev.hoffic.hc20e;

import dev.hoffic.hc20e.io.Dumper;
import dev.hoffic.hc20e.io.Parser;
import dev.hoffic.hc20e.logic.Simulator;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length == 1) {
            var problem = args[0];

            System.out.println("Parsing libraries...");
            var parser = new Parser();
            var world = parser.parse(problem);

            System.out.println("Running simulation...");
            var simulator = new Simulator();
            simulator.setWorld(world);
            simulator.simulate();

            System.out.println("Outputting result...");
            var dumper = new Dumper();
            dumper.dump(world, problem);
        } else {
            System.out.println("Wrong number of arguments.");
        }
    }
}
